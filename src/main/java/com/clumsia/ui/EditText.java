package com.clumsia.ui;

import com.clumsia.page.AppPage;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vbox;

/**
 * Created by Indocyber on 25/02/2018.
 */
public class EditText {
    private AppPage page;
    private String id;
    private String type;
    private String placeholder;
    private String label;
    private boolean readonly;

    public AppPage getPage() {
        return page;
    }
    public Hbox create(){
        Hbox layout=new Hbox();
        layout.setId(id);
        layout.setAlign("center");
        Vbox labelLayout=new Vbox();
        labelLayout.setWidth("100px");
        Label labelEl = new Label(label);
        labelEl.setParent(labelLayout);
        labelLayout.setParent(layout);
        Textbox txt=new Textbox();
        txt.setId(id+"-input");
        txt.setReadonly(readonly);
        txt.setPlaceholder(placeholder);
        switch (type){
            default:
                break;
            case "password":
                txt.setType("password");
                break;
            case "hidden":
                layout.setVisible(false);
                break;
        }
        txt.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                page.onEvent(PoseidonEvent.CHANGE,id);
            }
        });
        txt.setParent(layout);
        return layout;
    }
    public void setPage(AppPage page) {
        this.page = page;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public EditText(AppPage page, String id, String label, String type, String placeholder,boolean readonly){
        this.page=page;
        this.id=id;
        this.type=type;
        this.label=label;
        this.readonly=readonly;
        this.placeholder= placeholder;
    }
}

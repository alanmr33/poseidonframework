package com.clumsia.ui;

import com.clumsia.page.AppPage;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.A;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Span;

/**
 * Created by Indocyber on 27/02/2018.
 */
public class Button {
    private AppPage page;
    private String id;
    private String label;
    private String icon;
    private String color;

    public Button(AppPage page, String id, String label, String icon, String color) {
        this.page = page;
        this.id = id;
        this.label = label;
        this.icon = icon;
        this.color = color;
    }
    public Hbox create(){
        Hbox layout=new Hbox();
        A btn=new A();
        btn.setClass("posei-btn z-iconic");
        btn.setStyle("background : "+color);
        if(icon!=null && !icon.equals("")){
            Span iconBtn=FaIcon.create(icon,"white",1);
            iconBtn.setParent(btn);
            Label labelBtn=new Label(label);
            labelBtn.setParent(btn);
        }
        btn.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                page.onEvent(PoseidonEvent.CLICK,id);
            }
        });
        btn.setParent(layout);
        return layout;
    }

    public AppPage getPage() {
        return page;
    }

    public void setPage(AppPage page) {
        this.page = page;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}

package com.clumsia.ui;

/**
 * Created by Indocyber on 24/02/2018.
 */
public enum Color {
    BLUE("blue"),
    RED("Red"),
    DODGERBLUE("dodgerblue"),
    WHITE("white"),
    BLACK("black");
    private String color;
    Color(String color){
        this.color=color;
    }

    public String getColor() {
        return color;
    }
}

package com.clumsia.ui;

import com.clumsia.page.AppPage;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;

/**
 * Created by Indocyber on 26/02/2018.
 */
public class TimePicker {
    private AppPage page;
    private String id;
    private String label;
    private String placeholder;
    private boolean is24Hour;

    public AppPage getPage() {
        return page;
    }

    public void setPage(AppPage page) {
        this.page = page;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public boolean isIs24Hour() {
        return is24Hour;
    }

    public void setIs24Hour(boolean is24Hour) {
        this.is24Hour = is24Hour;
    }

    public TimePicker(AppPage page, String id, String label, String placeholder, boolean is24Hour) {
        this.page = page;
        this.id = id;
        this.label = label;
        this.placeholder = placeholder;
        this.is24Hour = is24Hour;

    }
    public Hbox create(){
        Hbox layout=new Hbox();
        layout.setId(id);
        layout.setAlign("center");
        Vbox labelLayout=new Vbox();
        labelLayout.setWidth("100px");
        Label labelEl = new Label(label);
        labelEl.setParent(labelLayout);
        labelLayout.setParent(layout);
        Timebox txt=new Timebox();
        txt.setId(id+"-input");
        txt.setPlaceholder(placeholder);
        if(is24Hour){
            txt.setFormat("hh:mm:ss");
        }else{
            txt.setFormat("hh:mm:ss a");
        }
        txt.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                page.onEvent(PoseidonEvent.CHANGE,id);
            }
        });
        txt.setParent(layout);
        return layout;
    }

}

package com.clumsia.richlet;

import com.clumsia.page.PageDashboard;
import com.clumsia.page.PageTest;
import com.clumsia.ui.Color;
import com.clumsia.ui.FaIcon;
import com.clumsia.ui.SimpleTimer;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;
import org.zkoss.zul.Image;

/**
 * Created by Indocyber on 24/02/2018.
 */
public class Home extends AppRichlet {
    private Center currentPage;
    @Override
    public void service(Page page) {
        super.service(page);
        try {
            if(session.getAttribute("token")==null){
                Executions.sendRedirect("/app/login");
            }else {
                page.setTitle("Poseidon | Home");

                Borderlayout layout = new Borderlayout();

                currentPage = new Center();
                currentPage.setStyle("min-width: 500px");
                currentPage.setTitle("Page");
                currentPage.setParent(layout);

                North north = new North();
                north.setClass("app-header");
                north.setHeight("75px");
                Hbox hbox = new Hbox();

                Image image = new Image("/img/zklogo1.png");
                image.setHeight("50px");
                image.setParent(hbox);

                Vbox menuTop = new Vbox();
                menuTop.setClass("menu-top");
                menuTop.setAlign("right");
                Div timer = SimpleTimer.create(page);
                timer.setParent(menuTop);
                Hbox menuList = new Hbox();
                menuList.setAlign("center");
                Span home = FaIcon.create("fa-home", Color.BLACK, 2);
                home.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        Executions.sendRedirect("/app/home");
                    }
                });
                home.setParent(menuList);
                Label currentLogin=new Label("Alan Maulana R");
                currentLogin.setParent(menuList);
                Span logout = FaIcon.create("fa-sign-out", Color.BLACK, 2);
                logout.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        session.removeAttribute("token");
                        Executions.sendRedirect("/app/login");
                    }
                });
                logout.setParent(menuList);
                menuList.setParent(menuTop);
                menuTop.setParent(hbox);
                hbox.setParent(north);
                north.setParent(layout);

                West west = new West();
                west.setTitle("Menu");
                west.setWidth("200px");
                west.setCollapsible(true);
                Menubar menubar = new Menubar();
                menubar.setOrient("vertical");
                Menuitem dashboard = new Menuitem();
                dashboard.setLabel("Dashboard");
                dashboard.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        currentPage.detach();
                        layout.detach();
                        currentPage = new PageDashboard(page).render();
                        currentPage.setParent(layout);
                        layout.setPage(page);
                    }
                });
                dashboard.setParent(menubar);

                Menu testMenu = new Menu();
                testMenu.setLabel("Test");
                Menupopup menupopup = new Menupopup();
                Menuitem childitem = new Menuitem();
                childitem.setLabel("Page Test");
                childitem.setParent(menupopup);
                childitem.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        currentPage.detach();
                        layout.detach();
                        currentPage = new PageTest(page).render();
                        currentPage.setParent(layout);
                        layout.setPage(page);
                    }
                });
                menupopup.setParent(testMenu);
                testMenu.setParent(menubar);
                menubar.setParent(west);
                west.setParent(layout);

                layout.setPage(page);
            }
        }catch (Exception e){
            System.err.println(e.getMessage());
            e.getStackTrace();
        }
    }
}

package com.clumsia.page;

import com.clumsia.ui.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.zkoss.zk.ui.Page;
import org.zkoss.zul.Hbox;

/**
 * Created by Indocyber on 24/02/2018.
 */
public class PageTest extends AppPage {
    public PageTest(Page page) {
        super(page);
    }
    @Override
    public void create() {
        container.setTitle("Page Test");
        JSONObject test=new JSONObject();
        JSONArray columns=new JSONArray();
        JSONArray data=new JSONArray();
        columns.put("A");
        columns.put("B");
        test.put("columns",columns);
        JSONObject data1=new JSONObject();
        data1.put("A","1");
        data1.put("B","2");
        JSONObject data2=new JSONObject();
        data2.put("A","3");
        data2.put("B","4");
        data.put(data1);
        data.put(data2);
        data.put(data1);
        data.put(data1);
        data.put(data1);
        data.put(data1);
        data.put(data1);
        data.put(data1);
        data.put(data1);
        data.put(data1);
        data.put(data1);
        data.put(data1);
        data.put(data1);
        data.put(data1);
        data.put(data1);
        data.put(data1);
        test.put("data",data);
        test.put("value","A");
        test.put("label","B");
        Row row=new Row();
        Hbox filter= new LOVInput(
                this,
                false,
                "single-dummy",
                "Single",
                "choose",
                "400px",
                test).create();
        Hbox filterMulti= new LOVInput(
                this,
                true,
                "multiple-dummy",
                "Multiple",
                "choose",
                "400px",
                test).create();
        Column column = new Column();
        column.addItem(filter);
        column.addItem(filterMulti);
        row.addItem(column.create());
        Hbox userID=new EditText(this,
                "userid",
                "UserID",
                "text",
                "UserID",
                true).create();
        row.addItem(userID);
        Hbox input=new EditText(this,
                "name",
                "Name",
                "text",
                "Name",
                false).create();
        row.addItem(input);
        Hbox inputPassword=new EditText(this,
                "password",
                "Password",
                "password",
                "Password",
                false).create();
        row.addItem(inputPassword);
        Hbox dateInput=new DatePicker(this,
                "datepicker",
                "Date Picker",
                "dd-MM-yyyyy",
                "dd-MM-yyyy").create();
        row.addItem(dateInput);
        Hbox timeInput=new TimePicker(this,
                "timepicker",
                "Time Picker",
                "hh:mm:ss",
                true).create();
        row.addItem(timeInput);
        JSONArray testA=new JSONArray();
        JSONObject active=new JSONObject("{value:\"active\",label:\"Active\"}");
        JSONObject pasive=new JSONObject("{value:\"pasive\",label:\"Pasive\"}");
        testA.put(active);
        testA.put(pasive);
        Hbox cmb=new Select(this,"select","Combo",testA).create();
        row.addItem(cmb);
        Hbox btn=new Button(this,"testbtn","Test","fa-users",Color.DODGERBLUE.getColor()).create();
        row.addItem(btn);
        row.create().setParent(content);
    }
}

package com.clumsia.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


/**
 * Created by Indocyber on 22/02/2018.
 */
@Controller
public class Home {
    @RequestMapping("/")
    public ModelAndView home(ModelMap model){
        return new ModelAndView("redirect:/app/login",model);
    }
    @RequestMapping("/app")
    public ModelAndView appHome(ModelMap model){
        return new ModelAndView("redirect:/app/home",model);
    }
}

package com.clumsia.utils;

import org.json.JSONObject;

import java.util.Comparator;

/**
 * Created by Indocyber on 25/02/2018.
 */
public class JSONObjectComparator implements Comparator<JSONObject> {
    private String columnName;
    public JSONObjectComparator(String columnName){
        this.columnName=columnName;
    }
    @Override
    public int compare(JSONObject o1, JSONObject o2) {
        String a=o1.get(columnName).toString();
        String b=o2.get(columnName).toString();
        return a.compareTo(b);
    }
}

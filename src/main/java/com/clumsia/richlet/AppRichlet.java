package com.clumsia.richlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.zkoss.zk.ui.GenericRichlet;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.RichletConfig;
import org.zkoss.zul.Script;
import org.zkoss.zul.Style;

import javax.servlet.http.HttpSession;

/**
 * Created by Indocyber on 24/02/2018.
 */
public class AppRichlet extends GenericRichlet {
    @Autowired
    public HttpSession session;
    @Override
    public void init(RichletConfig config) {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getWebApp().getServletContext());
    }

    @Override
    public void service(Page page) {
        Style fa = new Style();
        fa.setSrc("/css/font-awesome.min.css");
        fa.setPage(page);
        Style app = new Style();
        app.setSrc("/css/app.css");
        app.setPage(page);
    }
}

package com.clumsia.ui;

import org.zkoss.zul.Hbox;
import org.zkoss.zul.Vbox;

/**
 * Created by Indocyber on 27/02/2018.
 */
public class Row {
    private Vbox row;

    public Row() {
        this.row = new Vbox();
    }
    public Hbox create(){
        Hbox layout=new Hbox();
        row.setParent(layout);
        return layout;
    }
    public void addItem(Hbox item){
        item.setParent(row);
    }
}

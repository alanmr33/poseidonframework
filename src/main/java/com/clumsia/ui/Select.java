package com.clumsia.ui;

import com.clumsia.page.AppPage;
import org.json.JSONArray;
import org.json.JSONObject;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;

/**
 * Created by Indocyber on 27/02/2018.
 */
public class Select {
    private AppPage page;
    private String id;
    private String label;
    private JSONArray valueList;

    public Select(AppPage page, String id, String label, JSONArray valueList) {
        this.page = page;
        this.id = id;
        this.label = label;
        this.valueList = valueList;
    }
    public Hbox create(){
        Hbox layout=new Hbox();
        layout.setAlign("center");
        Vbox labelLayout=new Vbox();
        labelLayout.setWidth("100px");
        Label labelEl = new Label(label);
        labelEl.setParent(labelLayout);
        labelLayout.setParent(layout);
        Combobox cmb=new Combobox();
        cmb.setId(id+"-input");
        cmb.setWidth("213px");
        for (int i=0;i<valueList.length();i++){
            JSONObject object=valueList.getJSONObject(i);
            Comboitem comboitem = new Comboitem();
            comboitem.setValue(object.getString("value"));
            comboitem.setLabel(object.getString("label"));
            comboitem.setParent(cmb);
        }
        cmb.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                page.onEvent(PoseidonEvent.CHANGE,id);
            }
        });
        cmb.setParent(layout);
        return layout;
    }

    public AppPage getPage() {
        return page;
    }

    public void setPage(AppPage page) {
        this.page = page;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public JSONArray getValueList() {
        return valueList;
    }

    public void setValueList(JSONArray valueList) {
        this.valueList = valueList;
    }
}

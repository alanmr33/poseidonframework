package com.clumsia.page;

import com.clumsia.ui.PoseidonEvent;
import org.json.JSONArray;
import org.json.JSONObject;
import org.zkoss.zk.ui.Page;
import org.zkoss.zul.Center;
import org.zkoss.zul.Vlayout;

/**
 * Created by Indocyber on 24/02/2018.
 */
public class AppPage implements PageInterface, PoseidonEventHandler{
    public Page page;
    public Center container;
    public Vlayout content;
    public JSONObject events=new JSONObject();
    public AppPage(Page page){
        this.page=page;
        this.container=new Center();
        this.content=new Vlayout();
        this.content.setClass("main-content");
        this.content.setParent(container);
        this.create();
    }
    public void addEvent(String event, String id, JSONArray actionList){
        if(events.has(event)){
           this.addEventData(event, id,actionList);
        }else{
            events.put(event,new JSONObject());
            this.addEventData(event, id,actionList);
        }
    }
    private void addEventData(String event,String id ,JSONArray actionList){
        events.getJSONObject(event)
                .put(id,actionList);
    }
    @Override
    public void create() {

    }

    @Override
    public Center render() {
        return container;
    }

    @Override
    public void onEvent(PoseidonEvent event, String id) {

    }
}

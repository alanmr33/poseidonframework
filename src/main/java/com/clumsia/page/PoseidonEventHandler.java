package com.clumsia.page;

import com.clumsia.ui.PoseidonEvent;

/**
 * Created by Indocyber on 25/02/2018.
 */
public interface PoseidonEventHandler {
    void onEvent(PoseidonEvent event,String id);
}

package com.clumsia.ui;

import org.zkoss.zul.Span;

/**
 * Created by Indocyber on 24/02/2018.
 */
public abstract class FaIcon {
    public static Span create(String iconName,Color color,int scale){
        Span span=new Span();
        if(scale>1) {
            span.setClass("menu-icon fa " + iconName+" fa-"+scale+"x");
        }else{
            span.setClass("menu-icon fa " + iconName);
        }
        span.setStyle("color: "+color.getColor());
        return span;
    }
    public static Span create(String iconName,String color,int scale){
        Span span=new Span();
        if(scale>1) {
            span.setClass("menu-icon fa " + iconName+" fa-"+scale+"x");
        }else{
            span.setClass("menu-icon fa " + iconName);
        }
        span.setStyle("color: "+color);
        return span;
    }
}

package com.clumsia.ui;

import com.clumsia.page.AppPage;
import org.json.JSONArray;
import org.json.JSONObject;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.Window;
import java.util.List;
import java.util.Set;


/**
 * Created by Indocyber on 25/02/2018.
 */
public class LOVInput {
    private  boolean multiple;
    private  String id;
    private  String label;
    private  String button;
    private  String width;
    private  JSONObject properties;
    private AppPage page;
    public LOVInput(
                    AppPage page,
                    boolean multiple,
                    String id,
                    String label,
                    String button,
                    String width,
                    JSONObject properties){
        this.id=id;
        this.multiple=multiple;
        this.label=label;
        this.button=button;
        this.width=width;
        this.properties=properties;
        this.page=page;
    }
    public Hbox create(){
        JSONObject values=new JSONObject();
        Hbox layout=new Hbox();
        layout.setId(id);
        layout.setAlign("center");
        Vbox labelLayout=new Vbox();
        labelLayout.setWidth("100px");
        Label lovLabel = new Label(label);
        lovLabel.setParent(labelLayout);
        labelLayout.setParent(layout);
        Textbox txt=new Textbox();
        txt.setId(id+"-label");
        txt.setReadonly(true);
        txt.setParent(layout);
        Textbox hidden=new Textbox();
        hidden.setId(id+"-value");
        hidden.setVisible(false);
        hidden.setParent(layout);
        Button btn=new Button(button);
        btn.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                Window lovWindow=new Window();
                lovWindow.setStyle("min-width: 400px");
                lovWindow.setTitle(label);
                lovWindow.setClosable(true);
                Hbox filterLayout=new Hbox();
                filterLayout.setAlign("center");
                filterLayout.setWidth("100%");
                Vbox searchLayout=new Vbox();
                searchLayout.setWidth("100px");
                Label searchLabel = new Label("Search");
                searchLabel.setParent(filterLayout);
                Textbox filterText=new Textbox();
                filterText.setPlaceholder("Keywords");
                Span filterBtn=FaIcon.create("fa-search",Color.DODGERBLUE,2);
                filterText.setParent(filterLayout);
                filterBtn.setParent(filterLayout);
                filterLayout.setParent(lovWindow);
                Grid lovGrid=new Grid();
                lovGrid.setMold("paging");
                lovGrid.setPageSize(10);
                lovGrid.setWidth(width);
                Columns columns=new Columns();
                Rows rows=new Rows();
                JSONArray columnsList=new JSONArray();
                JSONArray data=new JSONArray();
                Checkbox checkboxAll = null;
                if(properties.has("columns")){
                    columnsList= (JSONArray) properties.get("columns");
                    if(multiple){
                        Column checkAll=new Column();
                        checkAll.setWidth("50px");
                        checkboxAll=new Checkbox();
                        checkboxAll.setId("checkAll");
                        checkboxAll.addEventListener(Events.ON_CHECK, new EventListener<Event>() {
                            @Override
                            public void onEvent(Event event) throws Exception {
                                Checkbox checkAll= (Checkbox) lovWindow.query("#checkAll");
                                List<Row> list=rows.getChildren();
                                for (int i=0;i<list.size();i++){
                                    if(checkAll.isChecked()) {
                                        values.put(String.valueOf(list.get(i).getIndex()),
                                                "checked");
                                        Checkbox chk= (Checkbox) list.get(i).getChildren().get(0);
                                        chk.setChecked(true);
                                    }else {
                                        values.remove(String.valueOf(list.get(i).getIndex()));
                                        Checkbox chk= (Checkbox) list.get(i).getChildren().get(0);
                                        chk.setChecked(false);
                                    }
                                }
                            }
                        });
                        checkboxAll.setParent(checkAll);
                        checkAll.setParent(columns);
                    }
                    for (int i=0;i<columnsList.length();i++){
                        Column column=new Column(columnsList.get(i).toString());
                        column.setAlign("center");
                        column.setSort("auto");
                        column.setParent(columns);
                    }
                }
                if(properties.has("data")){
                    data= properties.getJSONArray("data");
                    for (int i=0;i<data.length();i++){
                        Row row=new Row();
                        row.setAlign("center");
                        Checkbox checkbox = null;
                        if(multiple){
                            checkbox=new Checkbox();
                            checkbox.setId("chkrow-"+i);
                            if(values.has(String.valueOf(i))){
                                checkbox.setChecked(true);
                                if(values.length()==data.length()){
                                    checkboxAll.setChecked(true);
                                }
                            }
                            int finalI = i;
                            JSONArray finalData1 = data;
                            checkbox.addEventListener(Events.ON_CHECK, new EventListener<Event>() {
                                @Override
                                public void onEvent(Event event) throws Exception {
                                    Checkbox checkAll= (Checkbox) lovWindow.query("#checkAll");
                                    Checkbox check= (Checkbox) lovWindow.query("#chkrow-"+ finalI);
                                   if(check.isChecked()){
                                       values.put(String.valueOf(row.getIndex()),
                                               "checked");
                                       if(values.length()== finalData1.length()){
                                           checkAll.setChecked(true);
                                       }
                                   }else{
                                       values.remove(String.valueOf(row.getIndex()));
                                       checkAll.setChecked(false);
                                   }
                                }
                            });
                            checkbox.setParent(row);
                        }
                        for (int a=0;a<columnsList.length();a++){
                            String txtLbl=((JSONObject)data.get(i)).getString(columnsList.getString(a));
                            Label label1 = new Label(txtLbl);
                            label1.setParent(row);
                        }
                        int finalI1 = i;
                        JSONArray finalData2 = data;
                        row.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                            @Override
                            public void onEvent(Event event) throws Exception {
                                if(!multiple){
                                    txt.setText(finalData2.getJSONObject(row.getIndex())
                                            .getString(properties.getString("label")));
                                    hidden.setText(finalData2.getJSONObject(row.getIndex())
                                            .getString(properties.getString("value")));
                                    page.onEvent(PoseidonEvent.CHANGE,id);
                                    lovWindow.detach();
                                }else{
                                    Checkbox checkAll= (Checkbox) lovWindow.query("#checkAll");
                                    Checkbox check= (Checkbox) lovWindow.query("#chkrow-"+ finalI1);
                                    if(check.isChecked()){
                                        values.remove(String.valueOf(row.getIndex()));
                                        check.setChecked(false);
                                        checkAll.setChecked(false);
                                    }else{
                                        check.setChecked(true);
                                        values.put(String.valueOf(row.getIndex()),
                                                "checked");
                                        if(values.length()== finalData2.length()){
                                            checkAll.setChecked(true);
                                        }
                                    }
                                }
                            }
                        });
                        row.setParent(rows);
                    }
                }
                columns.setParent(lovGrid);
                rows.setParent(lovGrid);
                lovGrid.setParent(lovWindow);
                lovWindow.addEventListener(Events.ON_CLOSE, new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        if(multiple){
                            JSONArray data= (JSONArray) properties.get("data");
                            if(values.length()==data.length()){
                                txt.setText("All");
                                String checkedValue="";
                                Set<String> keys=values.keySet();
                                for (String s : keys) {
                                    checkedValue+=data.getJSONObject(Integer.parseInt(s))
                                            .getString(properties.getString("value"));
                                    checkedValue+=",";
                                }
                                if(checkedValue.length()>0) {
                                    char last = checkedValue.charAt(checkedValue.length() - 1);
                                    if (last == ',') {
                                        checkedValue = checkedValue.substring(0, checkedValue.length() - 1);
                                    }
                                }
                                hidden.setText(checkedValue);
                            }else{
                                String checked="";
                                String checkedValue="";
                                Set<String> keys=values.keySet();
                                for (String s : keys) {
                                    checked+=data.getJSONObject(Integer.parseInt(s))
                                            .getString(properties.getString("label"));
                                    checked+=",";
                                    checkedValue+=data.getJSONObject(Integer.parseInt(s))
                                            .getString(properties.getString("value"));
                                    checkedValue+=",";
                                }
                                if(checked.length()>0) {
                                    char last = checked.charAt(checked.length() - 1);
                                    if (last == ',') {
                                        checked = checked.substring(0, checked.length() - 1);
                                    }
                                }
                                if(checkedValue.length()>0) {
                                    char last = checkedValue.charAt(checkedValue.length() - 1);
                                    if (last == ',') {
                                        checkedValue = checkedValue.substring(0, checkedValue.length() - 1);
                                    }
                                }
                                txt.setText(checked);
                                hidden.setText(checkedValue);
                            }
                            page.onEvent(PoseidonEvent.CHANGE,id);
                        }
                    }
                });
                JSONArray finalColumnsList = columnsList;
                JSONArray finalData = data;
                filterBtn.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        if(multiple) {
                            Checkbox checkAll = (Checkbox) lovWindow.query("#checkAll");
                            checkAll.setChecked(false);
                        }
                        List<Row> list=rows.getChildren();
                        if(filterText.getText()!=null && !filterText.getText().equals("")) {
                            for (int i = 0; i < list.size(); i++) {
                                if(multiple){
                                    Checkbox chk = (Checkbox) list.get(i).getChildren().get(0);
                                    chk.setChecked(false);
                                }
                                boolean match = false;
                                for (int a = 0; a < finalColumnsList.length(); a++) {
                                    String txt = ((JSONObject) finalData.get(i)).getString(finalColumnsList.getString(a));
                                    if (filterText.getText().contains(txt)) {
                                        match = true;
                                    }
                                }
                                if (!match) {
                                    list.get(i).setVisible(false);
                                } else {
                                    list.get(i).setVisible(true);
                                }
                            }
                        }
                    }
                });
                lovWindow.setParent(layout);
                lovWindow.setMode(Window.Mode.MODAL);
            }
        });
        btn.setParent(layout);
        return layout;
    }

    public boolean isMultiple() {
        return multiple;
    }

    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public JSONObject getProperties() {
        return properties;
    }

    public void setProperties(JSONObject properties) {
        this.properties = properties;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

package com.clumsia.page;

import org.zkoss.zul.Center;

/**
 * Created by Indocyber on 24/02/2018.
 */
public interface PageInterface {
    public void create();
    public Center render();
}

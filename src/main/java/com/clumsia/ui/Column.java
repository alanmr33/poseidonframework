package com.clumsia.ui;

import org.zkoss.zul.Hbox;

/**
 * Created by Indocyber on 27/02/2018.
 */
public class Column {
    private Hbox column;

    public Column() {
        this.column = new Hbox();
    }
    public Hbox create(){
        Hbox layout=new Hbox();
        column.setParent(layout);
        return layout;
    }
    public void addItem(Hbox item){
        item.setParent(column);
    }
}

package com.clumsia.richlet;


import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;


public class Login extends AppRichlet {

	@Override
	public void service(Page page) {
		super.service(page);
		try {
			if(session.getAttribute("token")!=null){
				Executions.sendRedirect("/app/home");
			}else {
				page.setTitle("Poseidon | Login");

				Vlayout layout = new Vlayout();
				layout.setClass("login-page");
				layout.setWidth("100%");
				layout.setHeight("100%");

				Vbox form = new Vbox();
				form.setWidth("100%");
				form.setHeight("100%");
				form.setAlign("center");
				form.setPack("center");
				form.setParent(layout);

				Window window = new Window("", "normal", false);
				window.setStyle("margin-top: +200px");
				window.setWidth("400px");
				window.setHeight("240px");

				Image image = new Image("/img/zklogo1.png");
				image.setHeight("50px");
				image.setParent(window);

				Hlayout errorLayout = new Hlayout();
				errorLayout.setStyle("padding: 3px");
				errorLayout.setWidth("100%");
				Label errorTxt = new Label();
				errorTxt.setClass("error-txt");
				errorTxt.setParent(window);
				errorTxt.setParent(errorLayout);
				errorLayout.setParent(window);

				Hlayout idLayout = new Hlayout();
				idLayout.setWidth("100%");
				idLayout.setStyle("padding : 2px");
				Textbox txtid = new Textbox();
				txtid.setId("username");
				txtid.setWidth("200px");
				txtid.setPlaceholder("Username");
				txtid.setParent(idLayout);
				idLayout.setParent(window);

				Hlayout passLayout = new Hlayout();
				passLayout.setStyle("padding : 2px");
				passLayout.setWidth("100%");
				Textbox passid = new Textbox();
				passid.setId("password");
				passid.setWidth("200px");
				passid.setType("password");
				passid.setPlaceholder("Pasword");
				passid.setParent(passLayout);
				passLayout.setParent(window);

				Hlayout btnLayout = new Hlayout();
				btnLayout.setStyle("padding : 2px");
				btnLayout.setWidth("100%");
				Button btnLogin = new Button("SIGN IN");
				btnLayout.setWidth("100%");
				btnLogin.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
					@Override
					public void onEvent(Event event) throws Exception {
						if (txtid.getText().equals("alan")) {
							session.setAttribute("token", "aa");
							Executions.sendRedirect("/app/home");
						} else {
							errorTxt.setValue("Invalid Username & Password");
						}
					}
				});
				btnLogin.setParent(btnLayout);
				btnLayout.setParent(window);

				window.setParent(form);

				Label txtVersion = new Label("Versi 1.0");
				txtVersion.setStyle("color: white");
				txtVersion.setParent(form);
				layout.setPage(page);
			}
		}catch (Exception e){
			System.err.println(e.getMessage());
			e.getStackTrace();
		}
	}
}

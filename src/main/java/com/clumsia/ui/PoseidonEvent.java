package com.clumsia.ui;


/**
 * Created by Indocyber on 25/02/2018.
 */
public enum PoseidonEvent {
    CHANGE("change"),
    CLICK("click");
    private String event;
    PoseidonEvent(String event){
        this.event=event;
    }

    public String getEvent() {
        return event;
    }
}

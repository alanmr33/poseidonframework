package com.clumsia.ui;

import org.zkoss.zk.ui.Page;
import org.zkoss.zul.Div;
import org.zkoss.zul.Script;

/**
 * Created by Indocyber on 24/02/2018.
 */
public abstract class SimpleTimer {
    public static Div create(Page page){
        Script script=new Script();
        script.setSrc("/js/simpletimer.js");
        script.setPage(page);
        Div div=new Div();
        div.setClass("simple-timer");
        return div;
    }
}

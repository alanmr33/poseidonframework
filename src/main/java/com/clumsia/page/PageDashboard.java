package com.clumsia.page;

import org.zkoss.zk.ui.Page;

/**
 * Created by Indocyber on 25/02/2018.
 */
public class PageDashboard extends AppPage {
    public PageDashboard(Page page) {
        super(page);
    }
    @Override
    public void create() {
        container.setTitle("Dashboard");
    }
}
